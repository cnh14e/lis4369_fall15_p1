# LIS4369 FALL15 P1
## Cuong Huynh

==============================================

## Requirements

*    Display short assignment requirements
*    Display *your* name as “author”
*    Display current date/time (must include date/time, your format preference)
*    Must perform and display room size calculations, must include data validation and rounding to two decimal places.
*    Each data member must have get/set methods, also GetArea and GetVolume

==============================================

## Screenshots for Project 1

[![Screenshot 1](images/p1.png)](images/p1.png)

==============================================

## Links
[Bitbucket Repo](https://cnh14e@bitbucket.org/cnh14e/lis4369_fall15_p1)  

