﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace ConsoleApp2
{
    class Program
    {




        static void Main(string[] args)
        {

            Console.WriteLine("P1: Room Size Calculator - Using Classes");
            Console.WriteLine("Author: Cuong Huynh");
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));


            Console.WriteLine("\nNote: Expected values in feet.\n");

            //USING DEFAULT CONSTRUCTOR
            RoomCalculator defaultRoom = new RoomCalculator();

            //USING SETTERS AND GETTERS

            string rtype = "";
            double rlength = 0.0;
            double rwidth = 0.0;
            double rheight = 0.0;


            Console.WriteLine("Modify default constructor objects's data member values: ");
            Console.WriteLine("Use setter/getter method: ");
            Console.Write("Room Type: ");
            defaultRoom.setType(Console.ReadLine());
            Console.Write("Room Length: ");
            while(!double.TryParse(Console.ReadLine(), out rlength))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Length: ");
            }
            defaultRoom.setLength(rlength);

            //defaultRoom.setLength(Console.ReadLine());
            Console.Write("Room Width: ");
            //defaultRoom.setWidth(Console.ReadLine());
            while (!double.TryParse(Console.ReadLine(), out rwidth))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Width: ");
            }
            defaultRoom.setLength(rwidth);

            Console.Write("Room Height: ");
            //defaultRoom.setHeight(Console.ReadLine());

            while (!double.TryParse(Console.ReadLine(), out rheight))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Height: ");
            }
            defaultRoom.setLength(rheight);

            //DISPLAY SETTERS/GETTERS AND VOLUME/AREA VALUES

            Console.WriteLine("\nDisplay object's new data member values: ");
            Console.Write("Room Type: ");
            Console.WriteLine(defaultRoom.getType());
            Console.Write("Room Length: ");
            //Console.WriteLine(defaultRoom.getLength());
            Console.WriteLine(rlength);
            Console.Write("Room Width: ");
            //Console.WriteLine(defaultRoom.getWidth());
            Console.WriteLine(rwidth);
            Console.Write("Room Height: ");
            //Console.WriteLine(defaultRoom.getHeight());
            Console.WriteLine(rheight);

            Console.Write("Room Area: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findArea(rlength, rwidth), 2)
                + " sq ft");
            Console.Write("Room Volume: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findVolume(rlength, rwidth, rheight), 2)
                + " sq ft");
            Console.Write("Room Volume: ");
            double getterCubicYard = RoomCalculator.findCubicYard(rlength, rwidth, rheight);
            Console.WriteLine(String.Format("{0:0.00}", getterCubicYard) + " cu yd");
            Console.WriteLine();

            //VARIABLES FOR GETTERS AND CONVERSION
            /*
              string getterLengthString = defaultRoom.getLength();
              string getterWidthString = defaultRoom.getWidth();
              string getterHeightString = defaultRoom.getHeight();
              int getterLength;
              int getterWidth;
              int getterHeight;
              Int32.TryParse(getterLengthString, out getterLength);
              Int32.TryParse(getterWidthString, out getterWidth);
              Int32.TryParse(getterHeightString, out getterHeight);
            Console.Write("Room Area: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findArea(getterLength, getterWidth), 2)
                + " sq ft");
            Console.Write("Room Volume: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findVolume(getterLength, getterWidth, getterHeight), 2)
                + " sq ft");
            Console.Write("Room Volume: ");
            double getterCubicYard = RoomCalculator.findCubicYard(getterLength, getterWidth, getterHeight);
            Console.WriteLine(String.Format("{0:0.00}", getterCubicYard) + " cu yd");
            Console.WriteLine();
            */


            // PARAMATERIZED CONSTRUCTOR
            Console.WriteLine("Call parameterized constructor (accepts four arguments): ");
            Console.Write("Room Type: ");
            string roomType = Console.ReadLine();

            Console.Write("Room Length: ");

            while (!double.TryParse(Console.ReadLine(), out rlength))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Length: ");
            }

            //string lengthString;
            //int length;
            //Int32.TryParse(lengthString = Console.ReadLine(), out length);

            Console.Write("Room Width: ");

            while (!double.TryParse(Console.ReadLine(), out rwidth))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Width: ");
            }

            //string widthString;
            //int width;
            //Int32.TryParse(widthString = Console.ReadLine(), out width);

            Console.Write("Room Height: ");

            while (!double.TryParse(Console.ReadLine(), out rheight))
            {
                Console.WriteLine("Must be numeric value. ");
                Console.Write("Room Height: ");
            }
            //string heightString;
            //int height;
            //Int32.TryParse(heightString = Console.ReadLine(), out height);

            //USING PARAMETERIZED CONSTRUCTOR
            RoomCalculator paramConstructor = new RoomCalculator(roomType, rlength, rwidth, rheight);

            Console.Write("\nCreating Room object from parameterized constructor (accepts four arguments): \n");
            Console.WriteLine("\nDisplay object's new data member values: ");
            Console.WriteLine("Room Type: " + paramConstructor.getType());
            Console.WriteLine("Room Length: " + paramConstructor.getLength());
            Console.WriteLine("Room Width: " + paramConstructor.getWidth());
            Console.WriteLine("Room Height: " + paramConstructor.getHeight());

            Console.Write("Room Area: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findArea(rlength, rwidth), 2)
                + " sq ft");

            Console.Write("Room Volume: ");
            Console.WriteLine(String.Format("{0:0.00}", RoomCalculator.findVolume(rlength, rwidth, rheight), 2)
                + " sq ft");

            Console.Write("Room Volume: ");
            double cubicYard = RoomCalculator.findCubicYard(rlength, rwidth, rheight);
            Console.WriteLine(String.Format("{0:0.00}", cubicYard, 2) + " cu yd");
            Console.WriteLine();

            Console.WriteLine("\nPress Enter to exit ...\n");
            Console.ReadKey(true);

        }
    }
}
