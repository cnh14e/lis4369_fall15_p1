﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class RoomCalculator
    {
        //Private "access modifiers" for string variables
        private string roomType;
        private double length;
        private double width;
        private double height;

        public static double findArea(double L, double W)
        {
            double area;
            area = L * W;

            return area;
        }

        public static double findVolume(double L, double W, double H)
        {
            double volume;
            volume = L * W * H;

            return volume;
        }

        public static double findCubicYard(double L, double W, double H)
        {
            double cuYard;
            cuYard = (L * W * H) / 27;

            return cuYard;
        }


        // ACCESSOR SETTER / GETTER
        public string getType()
        {
            return roomType;
        }

        // MUTATOR SETTER / GETTER 
        public void setType(string type="default")
        {
            roomType = type;
        }


        // ACCESSOR SETTER / GETTER
        public double getLength()
        {
            return length;
        }

        // MUTATOR SETTER / GETTER 
        public void setLength(double L)
        {
            length = L;
        }

        // ACCESSOR SETTER / GETTER
        public double getWidth()
        {
            return width;
        }

        // MUTATOR SETTER / GETTER 
        public void setWidth(double W)
        {
            width = W;
        }

        public double getHeight()
        {
            return height;
        }

        // MUTATOR SETTER / GETTER 
        public void setHeight(double H)
        {
            height = H;
        }

        public RoomCalculator()
        {
            string defaultRoomType = "default room";
            double defaultLength = 10;
            double defaultWidth = 10;
            double defaultHeight = 10;
            double defaultArea = defaultLength * defaultWidth;
            double defaultVolumeSqFt = defaultLength * defaultWidth * defaultHeight;
            double defaultVolumeCuYd = (defaultLength * defaultWidth * defaultHeight) / 27;

            Console.WriteLine("Creating Room object from default constructor (accepts no arguments): ");
            Console.WriteLine("Room Type: " + defaultRoomType);
            Console.WriteLine("Room Length: " + defaultLength);
            Console.WriteLine("Room Width: " + defaultWidth);
            Console.WriteLine("Room Height: " + defaultHeight);
            Console.WriteLine("Room Area: " + defaultArea + " sq ft");
            Console.WriteLine("Room Volume: " + defaultVolumeSqFt + " cu ft");
            Console.WriteLine("Room Volume: " + String.Format("{0:0.00}", defaultVolumeCuYd, 2) + " cu yd");
            Console.WriteLine();
        }

        public RoomCalculator(string RT, double L, double W, double H)
        {
            roomType = RT;
            length = L;
            width = W;
            height = H;



        }
    }
}
